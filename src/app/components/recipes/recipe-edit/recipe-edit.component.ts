import { Recipe } from '../recipe.model';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from '../../../core/recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrl: './recipe-edit.component.css'
})
export class RecipeEditComponent implements OnInit{
  id: number;
  editMode = false;
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) {

  }

  ngOnInit() {
      this.route.params.subscribe(
        (prams: Params) => {
          this.id = +prams['id']
          this.editMode =  prams['id'] != null; 
          this.initForm();
        }
      );
  }

  private initForm() {
    
    let reciptName = '';
    let reciptImagePath = '';
    let reciptDescription = '';
    let recipeIngredients = new FormArray([]);

    if(this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);
      reciptName = recipe.name;
      reciptImagePath = recipe.imagePath;
      reciptDescription = recipe.description;
      if (recipe['ingredients']) {
        for(let indegrient of recipe.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(indegrient.name, Validators.required),
              'amount': new FormControl(indegrient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
            })
          );
        }
      }
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(reciptName, Validators.required),
      'imagePath': new FormControl(reciptImagePath, Validators.required),
      'description': new FormControl(reciptDescription, Validators.required),
      'ingredients': recipeIngredients
    })
  }

  onSubmit() {
    // const newRecipe = new Recipe(
    // this.recipeForm.value['name'],
    // this.recipeForm.value['description'],
    // this.recipeForm.value['imagePath'],
    // this.recipeForm.value['ingredients'],
    //  );
    if(this.editMode) {
      this.recipeService.updateRecipe(this.id, this.recipeForm.value);
    } else {
      this.recipeService.addRecipe(this.recipeForm.value);
    }
    this.onCancel();
  }

  get controls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    )
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route})
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }
  
}
