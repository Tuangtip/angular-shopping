import { NgModule } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";

const appRoutes: Routes = [
    { path: '', redirectTo: '/recipes', pathMatch: "full" },

];
@NgModule({
     imports: [RouterModule.forRoot(appRoutes)],
     exports: [RouterModule]
})
export class AppRoutingModule {
    
}