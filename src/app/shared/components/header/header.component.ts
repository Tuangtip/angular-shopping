import { Component, EventEmitter, Output } from '@angular/core';
import { DataStorageService } from '../../../core/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  constructor(private dataStroageService: DataStorageService) {}

  onSaveData() {
    this.dataStroageService.storeRecipes();
  }

  onFetchData() {
    this.dataStroageService.fetchRecipe().subscribe();
  }
}
