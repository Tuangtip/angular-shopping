import { RecipeService } from './recipe.service';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Recipe } from "../components/recipes/recipe.model";
import { map, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class DataStorageService {
    constructor(private http: HttpClient, private recipeService : RecipeService) {}

    storeRecipes() {
        const recipes = this.recipeService.getRecipes();
        return this.http.put('https://ng-course-recipe-book-4d685-default-rtdb.asia-southeast1.firebasedatabase.app/recipes.json', recipes).subscribe(
            response => {
                console.log("response", response);
                
            }
        );
    }

    fetchRecipe() {
        return this.http.get<Recipe[]>('https://ng-course-recipe-book-4d685-default-rtdb.asia-southeast1.firebasedatabase.app/recipes.json')
        .pipe(map(recipes => {
            return recipes.map(recipe => {
                return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
            });
        }),
        tap(recipes => {
            this.recipeService.setRecipes(recipes);
        })
        )
    }
}