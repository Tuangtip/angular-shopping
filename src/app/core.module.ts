import { NgModule } from "@angular/core";
import { ShoppingListService } from "./core/shopping-list.service";
import { RecipeService } from "./core/recipe.service";

@NgModule({
    providers: [
        ShoppingListService, 
        RecipeService
    ]
})
export class CoreModule {

}